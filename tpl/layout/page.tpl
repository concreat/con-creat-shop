[{capture append="oxidBlock_pageBody"}]
    [{if $oView->showRDFa()}]
        [{include file="rdfa/rdfa.tpl"}]
    [{/if}]

    [{block name="layout_header"}]
        [{include file="layout/header.tpl"}]
    [{/block}]

    [{assign var="blFullwidth" value=$oViewConf->getViewThemeParam('blFullwidthLayout')}]

    <div id="wrapper" [{if $sidebar}]class="sidebar[{$sidebar}]"[{/if}]>

        <div class="underdog">

            [{*//TODO Add option to switch between fullwidth slider and containered slider*}]
            <div class="container">
                <div class="row">
                [{if $oView->getClassName()=='start' && $oView->getBanners() && !empty($oView->getBanners())}]
                    [{include file="widget/promoslider.tpl"}]
                [{/if}]
                </div>
            </div>

            [{if $actCategory->oxcategories__oxthumb->value && $actCategory->getThumbUrl()}]
            <div class="image-wrapper">
                <img src="[{$oViewConf->getImageUrl('spinner.gif')}]" data-src="[{$actCategory->getThumbUrl()}]" alt="[{$actCategory->oxcategories__oxtitle->value}]" class="categoryPicture img-fluid">
            </div>
            [{/if}]

                <div class="content-box">
                    <div class="[{if $blFullwidth}]container[{else}]container-fluid[{/if}]">
                    [{$smarty.capture.loginErrors}]

                <div class="row">
                    [{if $sidebar && $sidebar != "Right"}]
                        <div class="col-12 col-md-4 col-lg-3 [{$oView->getClassName()}]">
                            <div id="sidebar">
                                [{include file="layout/sidebar.tpl"}]
                            </div>
                        </div>
                    [{/if}]

                    <div class="col-12[{if $sidebar}] col-md-8 col-lg-9[{/if}]">

                        <div class="content" id="content">
                            [{block name="content_main"}]
                                [{include file="message/errors.tpl"}]

                                [{foreach from=$oxidBlock_content item="_block"}]
                                    [{$_block}]
                                [{/foreach}]
                            [{/block}]
                        </div>

                    </div>

                    [{if $sidebar && $sidebar == "Right"}]
                        <div class="col-12 col-md-4 col-lg-3 [{$oView->getClassName()}]">
                            <div id="sidebar">
                                [{include file="layout/sidebar.tpl"}]
                            </div>
                        </div>
                    [{/if}]
                </div>

            </div>
            </div>
        </div>

    </div>

    [{include file="layout/footer.tpl"}]

    [{block name="layout_init_social"}]
    [{/block}]

    <i class="fa fa-chevron-circle-up icon-4x" id="jumptotop"></i>
[{/capture}]
[{include file="layout/base.tpl"}]
